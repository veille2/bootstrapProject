package fr.deeplearning.checkstyle;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Developper {

    private String name;

    private String age;
}
